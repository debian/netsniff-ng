netsniff-ng (0.6.9-1) unstable; urgency=medium

  * New upstream release:
    + Dropped upstream patch, merged upstream.

 -- Kartik Mistry <kartik@debian.org>  Wed, 08 Jan 2025 11:18:59 +0530

netsniff-ng (0.6.8-4) unstable; urgency=medium

  [ Chris Hofstaedtler ]
  * Recommends: add | time-daemon (Closes: #1001825)
  * Add upstream patch to fix makefile ordering (Closes: #1001825)
  * Replace obsolete Build-Depends: libncurses5-dev with libncurses-dev

  [ Kartik Mistry ]
  * Updated Standards-Version to 4.7.0

 -- Kartik Mistry <kartik@debian.org>  Tue, 07 Jan 2025 18:28:20 +0530

netsniff-ng (0.6.8-3) unstable; urgency=low

  * debian/control:
    + Recommends ntpsec instead of ntp.
    + Updated to Standards-Version 4.6.0
  * Simplified debian/watch file.

 -- Kartik Mistry <kartik@debian.org>  Fri, 08 Apr 2022 16:55:30 +0530

netsniff-ng (0.6.8-2) unstable; urgency=medium

  * debian/control:
    + Updated Uploaders as per MIA team request (Closes: #969983)

 -- Kartik Mistry <kartik@debian.org>  Tue, 12 Jan 2021 09:11:58 +0530

netsniff-ng (0.6.8-1) unstable; urgency=medium

  * Upload to unstable.
  * New upstream release:
    + Fix mausezahn build with GCC 10 (Closes: #957601)
  * debian/control:
    + Updated to Standards-Version 4.5.1
    + Switched to debhelper-compat.
    + Added 'Rules-Requires-Root' field.
  * Added debian/upstream/metadata file.
  * debian/rules:
    + Do not call dh_dwz, which generates unneeded debug files.

  [ Helmut Grohne ]
  * debian/rules:
    + Fix FTCBFS: Tell ./configure about the host architecture.
      (Closes: #914642)

 -- Kartik Mistry <kartik@debian.org>  Mon, 11 Jan 2021 18:36:02 +0530

netsniff-ng (0.6.6-1) experimental; urgency=low

  * New upstream release.
  * debian/control:
    + Updated to Standards-Version 4.3.0
  * debian/upstream/signing-key.asc:
    + Make it really usable.
  * Added debian/gitlab-ci.yml.

 -- Kartik Mistry <kartik@debian.org>  Mon, 20 May 2019 18:53:24 +0530

netsniff-ng (0.6.5-1) unstable; urgency=low

  * New upstream release.
  * debian/watch:
    + Updated to use https mirror from upstream.
    + Use upstream PGP sign key.
  * debian/control:
    + Updated to Standards-Version 4.2.1
  * debian/rules:
    + Remove empty directory.

 -- Kartik Mistry <kartik@debian.org>  Thu, 20 Sep 2018 18:36:14 +0530

netsniff-ng (0.6.4-2) unstable; urgency=low

  * debian/control:
    + Updated Vcs-* URLs.
    + Updated to Standards-Version 4.1.4
  * debian/rules
    + Updated for new dh style.
    + Fixed Hardening.

 -- Kartik Mistry <kartik@debian.org>  Mon, 18 Jun 2018 14:15:05 +0530

netsniff-ng (0.6.4-1) unstable; urgency=low

  * New upstream release:
    + Fix FTBFS error (Closes: #886385)
  * debian/control:
    + Updated to Standards-Version 4.1.3
  * Removed trailing whitespace from debian/changelog.
  * Update debian/compat to 11.

 -- Kartik Mistry <kartik@debian.org>  Sat, 06 Jan 2018 10:31:15 +0530

netsniff-ng (0.6.3-1) unstable; urgency=medium

  * New upstream release:
    + Fix FTBFS: error: 'GENL_ID_GENERATE' undeclared error (Closes: #867007)
  * debian/control:
    + Updated Standards-Version to 4.0.0
    + Fixed Vcs-Git URL.
  * debian/watch:
    + Updated to version 4.

 -- Kartik Mistry <kartik@debian.org>  Mon, 17 Jul 2017 13:04:10 +0530

netsniff-ng (0.6.2-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    + Updated Standards-Version to 3.9.8
  * debian/rules:
    + Removed empty dbg package.

 -- Kartik Mistry <kartik@debian.org>  Fri, 09 Dec 2016 20:52:29 +0530

netsniff-ng (0.6.1-2) unstable; urgency=low

  * debian/rules:
    + Unhide compiler flags.

 -- Kartik Mistry <kartik@debian.org>  Thu, 28 Apr 2016 09:07:46 +0530

netsniff-ng (0.6.1-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    + Updated Standards-Version to 3.9.7
    + Fixed Vcs-* URLs.
    + Updated long description.

 -- Kartik Mistry <kartik@debian.org>  Wed, 27 Apr 2016 10:58:43 +0530

netsniff-ng (0.6.0-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    + Added Build-Depends on libnl-route-3-dev
  * debian/copyright:
    + Added missing *.zsh files copyright.

 -- Kartik Mistry <kartik@debian.org>  Sat, 07 Nov 2015 12:42:13 +0530

netsniff-ng (0.5.9-3) unstable; urgency=low

  * Use libsodium instead of libnacl. Thanks to Tomasz Buchert
    <tomasz@debian.org> for patch.

 -- Kartik Mistry <kartik@debian.org>  Wed, 23 Sep 2015 17:03:50 +0530

netsniff-ng (0.5.9-2) unstable; urgency=low

  * debian/control:
    + Remove cmake from Build-Depends, as it's no longer used.
    + Fixed Vcs-Browser path.
    + Bumped debhelper dependency and debian/compat.
  * debian/rules:
    + Use verbose build to make compiler flags visible in buildd logs.

 -- Kartik Mistry <kartik@debian.org>  Sat, 22 Aug 2015 17:05:08 +0530

netsniff-ng (0.5.9-1) unstable; urgency=low

  * New upstream release:
    + Conditional operator does not typecheck (Closes: #750077)
  * debian/control:
    + Set arch:any (Closes: 749057)
    + Updated Standards-Version to 3.9.6
  * debian/copyright:
    + Updated copyright and fixed wildcard match.

 -- Kartik Mistry <kartik@debian.org>  Sat, 09 May 2015 20:13:45 +0530

netsniff-ng (0.5.8-2) unstable; urgency=low

  * debian/control:
    + Replaced ashunt with astraceroute in long description.
  * Updated debian/watch file.

 -- Kartik Mistry <kartik@debian.org>  Thu, 01 May 2014 14:54:51 +0530

netsniff-ng (0.5.8-1) unstable; urgency=low

  * New upstream release:
    + Dropped all patches. Merged upstream.
  * Uploaded to unstable.
  * debian/control:
    + Updated short description (Closes: #741997)
    + Added mausezahn in long description.
    + Added Tobias Klauser as Uploaders.
  * debian/rules:
    + Fixed installation as per upstream changes.
    + Removed unused dh_installexamples

 -- Kartik Mistry <kartik@debian.org>  Wed, 30 Apr 2014 21:12:59 +0530

netsniff-ng (0.5.8~rc5-2) experimental; urgency=low

  * debian/control:
    + Updated Daniel Borkmann's email address (Closes: #741327)
  * debian/rules:
    + Do not add PREFIX in make.
    + Do not remove config.h file in clean target.

 -- Kartik Mistry <kartik@debian.org>  Tue, 11 Mar 2014 16:12:59 +0530

netsniff-ng (0.5.8~rc5-1) experimental; urgency=low

  * New upstream RC release:
    + Removed patches merged in upstream.
    + Updated manpage_fix patch.
  * debian/control:
    + Added VCS-* fields.
    + Enabled ia64 and mips archs as liburcu-dev supports them now.
    + Updated build-depends to libnl-3-dev (Closes: #688171)
    + Updated Standards-Version to 3.9.5
  * debian/rules:
    + Fixed clean target.
    + Fixed hardening support.
  * debian/docs:
    + Documentation/ folder no longer exists.
  * Updated debian/watch file.

 -- Kartik Mistry <kartik@debian.org>  Mon, 10 Mar 2014 18:28:13 +0530

netsniff-ng (0.5.7-1) unstable; urgency=low

  * New upstream release
  * debian/copyright:
    + License is GPL-2, not GPL-2+
  * debian/control:
    + Added Build-Depends: libcli-dev, libnl-dev for trafgen, netsniff-ng
      binaries.
    + Updated maintainer email address.
  * debian/patches/cflags_notune.patch:
    + Added patch to fix CFLAGS from upstream.
  * debian/docs:
    + Removed THANKS, not provided by upstream now.
  * debian/rules:
    + Provides upstream changelogs.

 -- Kartik Mistry <kartik@debian.org>  Sat, 30 Jun 2012 11:52:47 +0530

netsniff-ng (0.5.6-2) unstable; urgency=low

  * debian/control:
    + Set archs to: i386, amd64, powerpc and sparc only as dependencies are not
      available or missing on other than these archs.
    + Added Recommends: ntp. Thanks to Carlos Alberto Lopez Perez
      <clopez@igalia.com>.
    + Build-Depends on libncurses5-dev for missing flowtop and ifpps binaries.
  * debian/copyright:
    + Removed duplicate copyright fields.
    + Added Debian packaging copyright section.
  * Added hardening support.

 -- Kartik Mistry <kartik@debian.org>  Thu, 21 Jun 2012 16:36:41 +0530

netsniff-ng (0.5.6-1) unstable; urgency=low

  * New upstream release (Closes: #667609)
  * debian/patches:
    + 01_manpage_fix.diff: Removed, merge upstream.
    + nacl_path_fix: Added patch to fix Nacl header and library paths.
    + manpage_path.patch: Added patch to fix manpage installation path from
      upstream.
  * debian/rules:
    + Upstream moved to CMake. Changes related to it.
    + Added missing build targets.
  * debian/control:
    + Added Build-Deps needed for CMake build system and libs.
    + Updated Standards-Version to 3.9.3
    + Updated long description.
  * debian/copyright:
    + Updated to copyright-format 1.0
    + Updated copyright for some files.
  * debian/docs:
    + Updated as per upstream changes.

 -- Kartik Mistry <kartik@debian.org>  Sun, 01 Apr 2012 11:05:41 +0530

netsniff-ng (0.5.5.0-3) unstable; urgency=low

  * debian/control:
    + Updated Standards-Version to 3.9.2
    + Fixed short description synopsis
  * debian/copyright:
    + Updated for DEP-5 format specification
  * Added patch to fix spelling mistake in manpage

 -- Kartik Mistry <kartik@debian.org>  Mon, 30 May 2011 08:49:15 +0530

netsniff-ng (0.5.5.0-2) unstable; urgency=low

  * Upload to unstable
  * debian/control:
    + Added armhf support, closes wishlist bug. Closes: #604540
    + Excluded non-linux systems in architectures
    + Added Kartik as Uploader

 -- Daniel Borkmann <danborkmann@googlemail.com>  Mon, 07 Feb 2011 16:26:17 +0200

netsniff-ng (0.5.5.0-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    + Updated to new standards version 3.9.1
    + Updated description
  * debian/watch:
    + Updated URL to public directory of netsniff-ngs website
  * Switch to dpkg-source 3.0 (quilt) format

 -- Daniel Borkmann <danborkmann@googlemail.com>  Sun, 10 Oct 2010 12:57:11 +0200

netsniff-ng (0.5.4.2-1) unstable; urgency=low

  * Another minor upstream release with fixes within the ring traversal
    method (packet look-ahead on the receive ring) and some basic BPF
    filter check.
  * Changed installation path from /sbin into /usr/sbin
  * debian/control:
    + Updated to new standards version 3.8.4
    + Added ${misc:Depends} in order to fix the 'debhelper-but-no-misc-depends'
      warning

 -- Daniel Borkmann <danborkmann@googlemail.com>  Sun, 14 Feb 2010 13:58:40 +0200

netsniff-ng (0.5.4.1-1) unstable; urgency=medium

  * New (minor) upstream release with security fix in netsniff-ng.c:
    A memset with a 4 Byte length overhead on 32-Bit systems could run into a
    possible buffer overflow. Relevant sections have been fixed.
  * debian/control:
    + Changed architecture, because netsniff-ng is not written for non-linux
      archs as kfreebsd-*

 -- Daniel Borkmann <danborkmann@googlemail.com>  Sat, 02 Jan 2010 11:18:03 +0200

netsniff-ng (0.5.4.0-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    + Corrected package description
  * debian/watchfile:
    + Changed URL to watch for, because the googlecode site denies access,
      mirrored tarball archive at htwk-leipzig.de

 -- Daniel Borkmann <danborkmann@googlemail.com>  Fri, 01 Jan 2010 19:07:23 +0200

netsniff-ng (0.5.3.1-1) unstable; urgency=low

  * Initial release for Debian, closes ITP bug. Closes: #561528

 -- Daniel Borkmann <danborkmann@googlemail.com>  Thu, 17 Dec 2009 22:55:51 +0200
